package com.squaredevops.darkmodetest2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    SwitchCompat switchCompat;
    SharedPreferences sharedPreferences = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.imageView);
        switchCompat = findViewById(R.id.switchCompact);


        sharedPreferences = getSharedPreferences("night_mode", MODE_PRIVATE); // save file name

        // default theme
        /*
        Boolean defaultValue = sharedPreferences.getBoolean("mode",true); // string name
        if (defaultValue){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES); // night mode
            switchCompat.setChecked(true); // by default
            imageView.setImageResource(R.drawable.night); // image for night mode
        }
        */

        // onclick
        switchCompat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                    // night theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES); // setting night theme
                    switchCompat.setChecked(true); // if checked false
                    imageView.setImageResource(R.drawable.night); // image for night theme

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("mode",true); // put-key for save as string
                    editor.apply(); // commit

                } else {

                    // light theme
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO); // setting light theme
                    switchCompat.setChecked(false); // if checked true
                    imageView.setImageResource(R.drawable.day); // image for light theme

                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putBoolean("mode",false); // put-key for save as string
                    editor.apply(); // commit

                }
            }
        });

    }
}